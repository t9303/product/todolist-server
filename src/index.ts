import 'reflect-metadata';
import { WebSocketServer } from 'ws';
import { Disposable } from 'graphql-ws';
import { GraphQLSchema } from 'graphql';
import { createConnection } from 'typeorm';
import { createServer, Server } from 'http';
import { useServer } from 'graphql-ws/lib/use/ws';
import { ApolloServer } from 'apollo-server-express';
import express, { Application, Router } from 'express';
import { buildSchema, NonEmptyArray, ClassType } from 'type-graphql';
import './config';
import ormConfig from './config/orm';
import * as routers from './routers';
import {
	buildContext,
	getScopedContainer
} from './common/management/Apollo/context';
import LoggerProvider from './config/logger';
import * as resolvers from './schemas/resolvers';
import {
	drainWSServer,
	drainHttpServer,
	drainContainerInstance
} from './common/management/Apollo/apolloPlugins';

const main = async () => {
	const app: Application = express();
	const httpServer: Server = createServer(app);
	const wsServer = new WebSocketServer({
		server: httpServer,
		path: '/graphql'
	});

	Object.values(routers).forEach((router: Router) => {
		app.use(router);
	});

	await createConnection(ormConfig);

	const schema: GraphQLSchema = await buildSchema({
		container: getScopedContainer,
		resolvers: <NonEmptyArray<ClassType>>Object.values(resolvers),
		authChecker: () => true
	});

	const wsServerCleanup: Disposable = useServer({ schema }, wsServer);
	const apolloServer = new ApolloServer({
		schema,
		context: buildContext,
		plugins: [
			drainHttpServer({ httpServer }),
			drainWSServer({ wsServerCleanup }),
			drainContainerInstance()
		]
	});

	await apolloServer.start();
	apolloServer.applyMiddleware({ app, cors: false });

	httpServer.listen(4000, () => {
		LoggerProvider.logger.info('express server started');
	});
};

main().catch((error: Error) => {
	LoggerProvider.logger.error(`express server failed to start`, error);
});
