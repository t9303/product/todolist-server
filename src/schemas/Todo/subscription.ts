import { Field, ObjectType } from 'type-graphql';
import Todo from './entity';
import { SubscriptionActions } from '../../common/models';

export interface TodoSubscriptionPayload {
	todoId: string;
	action: SubscriptionActions;
}

@ObjectType()
export class TodoSubscription {
	@Field(() => SubscriptionActions)
	action!: SubscriptionActions;

	@Field(() => Todo, { nullable: true })
	todo?: Todo;

	constructor(action: SubscriptionActions, todo?: Todo) {
		this.todo = todo;
		this.action = action;
	}
}
