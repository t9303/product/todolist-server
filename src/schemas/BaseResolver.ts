import { Logger } from 'winston';
import { ClassType } from 'type-graphql';
import LoggerProvider from '../config/logger';

export default class BaseResolver {
	protected logger: Logger;

	constructor(objectType: ClassType, tenantLogger?: Logger) {
		this.logger = (tenantLogger || LoggerProvider.logger).child({
			objectType: objectType.name
		});
	}
}
