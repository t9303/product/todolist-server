import { registerEnumType } from 'type-graphql';

export enum Status {
	All = -1,
	Active,
	Completed
}

registerEnumType(Status, {
	name: 'Status'
});
