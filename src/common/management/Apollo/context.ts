import { ContainerGetter } from 'type-graphql';
import { Request as ExpressRequest } from 'express';
import { ContextFunction } from 'apollo-server-core';
import { Container, ContainerInstance } from 'typedi';
import { ExpressContext } from 'apollo-server-express';
import { Connection, Equal, getConnection } from 'typeorm';
import { RLSConnection } from '../RLS';
import { IContext } from '../../models';
import { Tenant } from '../../../schemas/entities';
import LoggerProvider from '../../../config/logger';
import { CONNECTION_DI_KEY, LOGGER_DI_KEY, numbersRegexp } from '../../consts';

export const buildTenantContainer = async (
	req: ExpressRequest,
	container: ContainerInstance
): Promise<void> => {
	const tenantId = <string | undefined>req.headers['tenant-id'];
	const defaultConnection: Connection = getConnection();

	if (!tenantId) {
		return;
	}

	if (numbersRegexp.test(tenantId)) {
		try {
			const { id } = await Tenant.findOneOrFail({
				where: {
					id: Equal(parseInt(tenantId))
				}
			});

			container.set(
				CONNECTION_DI_KEY,
				new RLSConnection(defaultConnection, {
					tenantId: id
				})
			);
			container.set(
				LOGGER_DI_KEY,
				LoggerProvider.logger.child({ tenantId: id })
			);
		} catch (error) {
			throw new Error(`The requested tenant '${tenantId}' doesn't exist`);
		}
	} else {
		throw new Error(`The requested tenant '${tenantId}' is invalid`);
	}
};

export const buildContext: ContextFunction<ExpressContext> = async ({
	req
}): Promise<IContext> => {
	const containerId: string = Math.floor(
		Math.random() * Number.MAX_SAFE_INTEGER
	).toString();

	const container = Container.of(containerId);
	await buildTenantContainer(req, container);

	return { req, containerId };
};

export const getScopedContainer: ContainerGetter<IContext> = ({
	context: { containerId }
}) => Container.of(containerId);
