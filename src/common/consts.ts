export const LOGGER_DI_KEY: string = 'logger';
export const CONNECTION_DI_KEY: string = 'connection';
export const isDev: boolean = process.env.NODE_ENV === 'development';
export const numbersRegexp: RegExp = /^\d+$/;
